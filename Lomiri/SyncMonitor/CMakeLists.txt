set(SYNC_MONITOR_QML_SRC
    plugin.h
    plugin.cpp
    syncmonitor-qml.h
    syncmonitor-qml.cpp
)

set(SYNC_MONITOR_QML_FILES
    qmldir
)

add_library(syncmonitor-qml SHARED ${SYNC_MONITOR_QML_SRC})
target_link_libraries(syncmonitor-qml PUBLIC
    Qt5::Core
    Qt5::DBus
    Qt5::Qml
)

set(QT_INSTALL_QML "${CMAKE_INSTALL_LIBDIR}/qt5/qml")

set(SYNC_MONITOR_QML_INSTALL_DIR ${QT_INSTALL_QML}/Lomiri/SyncMonitor)
install(TARGETS syncmonitor-qml DESTINATION ${SYNC_MONITOR_QML_INSTALL_DIR})
install(FILES ${SYNC_MONITOR_QML_FILES} DESTINATION ${SYNC_MONITOR_QML_INSTALL_DIR})

# make the files visible on qtcreator
add_custom_target(syncmonitor_QmlFiles ALL SOURCES ${SYNC_MONITOR_QML_FILES})
