/*
 * Copyright 2014 Canonical Ltd.
 * Copyright 2023 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * This file is part of lomiri-sync-monitor.
 *
 * lomiri-sync-monitor is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sync-network.h"

#include <QDebug>
#include <QTimer>
#include <QDBusConnection>

SyncNetwork::SyncNetwork(QObject *parent)
    : QObject(parent),
      m_connectivity(new connectivityqt::Connectivity(QDBusConnection::sessionBus(), this)),
      m_state(SyncNetwork::NetworkOffline)
{
    connect(m_connectivity.data(), &connectivityqt::Connectivity::onlineUpdated, this, &SyncNetwork::onOnlineUpdated);
    connect(m_connectivity.data(), &connectivityqt::Connectivity::limitedBandwidthUpdated, this, &SyncNetwork::onLimitedBandwidthUpdated);

    m_idleRefresh.setSingleShot(true);
    m_idleRefresh.start(1000);
    connect(&m_idleRefresh, &QTimer::timeout, this, &SyncNetwork::updateState);
}

SyncNetwork::~SyncNetwork()
{
}

SyncNetwork::NetworkState SyncNetwork::state() const
{
    return m_state;
}

void SyncNetwork::setState(SyncNetwork::NetworkState newState)
{
    if (m_state != newState) {
        m_state = newState;
        qDebug() << "Network state changed:" << (m_state == SyncNetwork::NetworkOffline ? "Offline" :
                                                 m_state == SyncNetwork::NetworkPartialOnline ? "Partial online" : "Online");
        Q_EMIT stateChanged(m_state);
    }
}

void SyncNetwork::updateState()
{
    if (m_isOnline && m_isLimitedBandwidth) {
        setState(SyncNetwork::NetworkPartialOnline);
    } else if (m_isOnline) {
        setState(SyncNetwork::NetworkOnline);
    } else {
        setState(SyncNetwork::NetworkOffline);
    }
}

void SyncNetwork::onOnlineUpdated(bool newIsOnline)
{
    if (m_isOnline != newIsOnline) {
        m_isOnline = newIsOnline;
        updateState();
    }
}

void SyncNetwork::onLimitedBandwidthUpdated(bool newIsLimitedBandwidth)
{
    if (m_isLimitedBandwidth != newIsLimitedBandwidth) {
        m_isLimitedBandwidth = newIsLimitedBandwidth;
        updateState();
    }
}
