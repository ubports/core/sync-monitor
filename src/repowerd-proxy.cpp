/*
 * Copyright 2016 Canonical Ltd.
 *
 * This file is part of lomiri-sync-monitor.
 *
 * lomiri-sync-monitor is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repowerd-proxy.h"

#include <QtCore/QDebug>
#include <QtCore/QUuid>
#include <QtDBus/QDBusReply>

#define REPOWERD_SERVICE_NAME     "com.lomiri.Repowerd"
#define REPOWERD_IFACE_NAME       "com.lomiri.Repowerd"
#define REPOWERD_OBJECT_PATH      "/com/lomiri/Repowerd"

RepowerdProxy::RepowerdProxy(QObject *parent)
    : QObject(parent)
{
    m_iface = new QDBusInterface(REPOWERD_SERVICE_NAME,
                                 REPOWERD_OBJECT_PATH,
                                 REPOWERD_IFACE_NAME,
                                 QDBusConnection::systemBus());
}

RepowerdProxy::~RepowerdProxy()
{
    unlock();
}

QString RepowerdProxy::requestWakelock(const QString &name) const
{
    QDBusReply<QString> reply = m_iface->call("requestSysState", name, 1);
    if (reply.error().isValid()) {
        qWarning() << "Fail to request wake lock" << reply.error().message();
        return QString();
    }

    return reply.value();
}

bool RepowerdProxy::clearWakelock(const QString &cookie) const
{
    QDBusReply<void> reply = m_iface->call("clearSysState", cookie);
    if (reply.error().isValid()) {
        qWarning() << "Fail to clear wake lock" << reply.error().message();
        return false;
    }
    return true;
}

void RepowerdProxy::lock()
{
    if (!m_currentLock.isEmpty()) {
        qDebug() << "Wake lock already created for lomiri-sync-monitor";
        return;
    }

    QString cookie = requestWakelock("lomiri-sync-monitor-wakelock");
    if (!cookie.isEmpty()) {
        m_currentLock = cookie;
    }
}

void RepowerdProxy::unlock()
{
    if (!m_currentLock.isEmpty() &&  clearWakelock(m_currentLock)) {
        m_currentLock.clear();
    }
}
