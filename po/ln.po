# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the sync-monitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: sync-monitor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 09:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ln\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: accounts/plugin/generic-caldav.provider.in:3
msgid "Generic CalDAV"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:15
msgid "Invalid host URL"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:38
msgid "URL:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:44
msgid "http://myserver.com/caldav/"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:53
msgid "Username:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:59
msgid "Your username"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:68
msgid "Password:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:74
msgid "Your password"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:89
msgid "Cancel"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:95
msgid "Continue"
msgstr ""

#: accounts/services/generic-caldav.service.in:4
msgid "Generic Calendar"
msgstr ""

#: accounts/services/google-caldav.service.in:4
msgid "Google Calendar"
msgstr ""

#: accounts/services/google-carddav.service.in:4
msgid "Google Contacts"
msgstr ""

#: accounts/services/nextcloud-caldav.service.in:4
#: accounts/services/owncloud-caldav.service.in:4
msgid "Calendar"
msgstr ""

#: authenticator/main.qml:60 authenticator/main.qml:78
msgid "Accounts"
msgstr ""

#: authenticator/main.qml:82 authenticator/main.qml:130
msgid "Quit"
msgstr ""

#: authenticator/main.qml:88
msgid "Could not load account info."
msgstr ""

#: authenticator/main.qml:126
msgid "Logging in…"
msgstr ""

#: authenticator/main.qml:126
msgid "Sign in to sync"
msgstr ""

#: authenticator/main.qml:153
msgid "Account sign-in failed. Please select your account to sign back in."
msgstr ""

#: src/notify-message.cpp:86
msgid "Yes"
msgstr ""

#: src/notify-message.cpp:91
msgid "No"
msgstr ""

#: src/sync-account.cpp:735
msgid "Failed to configure account"
msgstr ""

#: src/sync-account.cpp:744
msgid "Forbidden / access denied"
msgstr ""

#: src/sync-account.cpp:746
msgid "Object not found / unassigned field"
msgstr ""

#: src/sync-account.cpp:748
msgid "Command not allowed"
msgstr ""

#: src/sync-account.cpp:751
msgid "Proxy authentication required"
msgstr ""

#: src/sync-account.cpp:753
msgid "Disk full"
msgstr ""

#: src/sync-account.cpp:755
msgid "Could not sync due to a remote problem"
msgstr ""

#: src/sync-account.cpp:757
msgid "Could not run \"two-way\" sync"
msgstr ""

#: src/sync-account.cpp:759
msgid "Could not sync some items"
msgstr ""

#: src/sync-account.cpp:761
msgid "Process died unexpectedly"
msgstr ""

#: src/sync-account.cpp:764
msgid "Server sent bad content"
msgstr ""

#: src/sync-account.cpp:766
msgid "Sync canceled"
msgstr ""

#: src/sync-account.cpp:768
msgid "Connection timeout"
msgstr ""

#: src/sync-account.cpp:770
msgid "Connection certificate has expired"
msgstr ""

#: src/sync-account.cpp:772
msgid "Connection certificate is invalid"
msgstr ""

#: src/sync-account.cpp:776
msgid "Could not connect to the server"
msgstr ""

#: src/sync-account.cpp:779
msgid "Server not found"
msgstr ""

#: src/sync-account.cpp:781
msgid "Unknown status"
msgstr ""

#: src/sync-daemon.cpp:627 src/sync-daemon.cpp:649 src/sync-daemon.cpp:759
#: src/sync-daemon.cpp:778
msgid "Synchronization"
msgstr ""

#: src/sync-daemon.cpp:628
msgid "An account failed to sync. Would you like to sign in again?"
msgstr ""

#: src/sync-daemon.cpp:651
msgid "Syncing %1 (Calendar)"
msgstr ""

#: src/sync-daemon.cpp:760
msgid ""
"Could not sync calendar %1 from account %2.\n"
"%3"
msgstr ""

#: src/sync-daemon.cpp:780
msgid "Finished syncing %1 (Calendar)"
msgstr ""
