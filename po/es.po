# Spanish translation for sync-monitor
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the sync-monitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: sync-monitor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 09:52+0000\n"
"PO-Revision-Date: 2023-02-16 22:38+0000\n"
"Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/lomiri/sync-"
"monitor/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2016-12-09 05:39+0000\n"

#: accounts/plugin/generic-caldav.provider.in:3
msgid "Generic CalDAV"
msgstr "CalDAV genérico"

#: accounts/plugin/qml/NewAccount.qml:15
msgid "Invalid host URL"
msgstr "URL de anfitrión no válido"

#: accounts/plugin/qml/NewAccount.qml:38
msgid "URL:"
msgstr "URL:"

#: accounts/plugin/qml/NewAccount.qml:44
msgid "http://myserver.com/caldav/"
msgstr "http://miservidor.com/caldav/"

#: accounts/plugin/qml/NewAccount.qml:53
msgid "Username:"
msgstr "Nombre de usuario:"

#: accounts/plugin/qml/NewAccount.qml:59
msgid "Your username"
msgstr "Su nombre de usuario"

#: accounts/plugin/qml/NewAccount.qml:68
msgid "Password:"
msgstr "Contraseña:"

#: accounts/plugin/qml/NewAccount.qml:74
msgid "Your password"
msgstr "Su contraseña"

#: accounts/plugin/qml/NewAccount.qml:89
msgid "Cancel"
msgstr "Cancelar"

#: accounts/plugin/qml/NewAccount.qml:95
msgid "Continue"
msgstr "Continuar"

#: accounts/services/generic-caldav.service.in:4
msgid "Generic Calendar"
msgstr "Calendario genérico"

#: accounts/services/google-caldav.service.in:4
msgid "Google Calendar"
msgstr "Calendario de Google"

#: accounts/services/google-carddav.service.in:4
msgid "Google Contacts"
msgstr "Contactos de Google"

#: accounts/services/nextcloud-caldav.service.in:4
#: accounts/services/owncloud-caldav.service.in:4
msgid "Calendar"
msgstr "Calendario"

#: authenticator/main.qml:60 authenticator/main.qml:78
msgid "Accounts"
msgstr "Cuentas"

#: authenticator/main.qml:82 authenticator/main.qml:130
msgid "Quit"
msgstr "Salir"

#: authenticator/main.qml:88
msgid "Could not load account info."
msgstr "No se pudo cargar la información de la cuenta."

#: authenticator/main.qml:126
msgid "Logging in…"
msgstr "Accediendo…"

#: authenticator/main.qml:126
msgid "Sign in to sync"
msgstr "Acceder para sincronizar"

#: authenticator/main.qml:153
msgid "Account sign-in failed. Please select your account to sign back in."
msgstr ""
"Falló el acceso a la cuenta. Seleccione su cuenta para volver a acceder."

#: src/notify-message.cpp:86
msgid "Yes"
msgstr "Sí"

#: src/notify-message.cpp:91
msgid "No"
msgstr "No"

#: src/sync-account.cpp:735
msgid "Failed to configure account"
msgstr "No se pudo configurar la cuenta"

#: src/sync-account.cpp:744
msgid "Forbidden / access denied"
msgstr "Prohibido/acceso denegado"

#: src/sync-account.cpp:746
msgid "Object not found / unassigned field"
msgstr "Objeto no encontrado/campo sin asignar"

#: src/sync-account.cpp:748
msgid "Command not allowed"
msgstr "Orden no permitida"

#: src/sync-account.cpp:751
msgid "Proxy authentication required"
msgstr "Se necesita autenticación del «proxy»"

#: src/sync-account.cpp:753
msgid "Disk full"
msgstr "Disco lleno"

#: src/sync-account.cpp:755
msgid "Could not sync due to a remote problem"
msgstr "No se pudo sincronizar debido a un problema remoto"

#: src/sync-account.cpp:757
msgid "Could not run \"two-way\" sync"
msgstr "No se pudo ejecutar la sincronización «de dos vías»"

#: src/sync-account.cpp:759
msgid "Could not sync some items"
msgstr "No se pudieron sincronizar algunos elementos"

#: src/sync-account.cpp:761
msgid "Process died unexpectedly"
msgstr "El proceso terminó de forma inesperada"

#: src/sync-account.cpp:764
msgid "Server sent bad content"
msgstr "El servidor envió contenido erróneo"

#: src/sync-account.cpp:766
msgid "Sync canceled"
msgstr "Sincronización cancelada"

#: src/sync-account.cpp:768
msgid "Connection timeout"
msgstr "Expiró la conexión"

#: src/sync-account.cpp:770
msgid "Connection certificate has expired"
msgstr "El certificado de la conexión caducó"

#: src/sync-account.cpp:772
msgid "Connection certificate is invalid"
msgstr "El certificado de la conexión no es válido"

#: src/sync-account.cpp:776
msgid "Could not connect to the server"
msgstr "No se pudo conectar con el servidor"

#: src/sync-account.cpp:779
msgid "Server not found"
msgstr "Servidor no encontrado"

#: src/sync-account.cpp:781
msgid "Unknown status"
msgstr "Estado desconocido"

#: src/sync-daemon.cpp:627 src/sync-daemon.cpp:649 src/sync-daemon.cpp:759
#: src/sync-daemon.cpp:778
msgid "Synchronization"
msgstr "Sincronización"

#: src/sync-daemon.cpp:628
msgid "An account failed to sync. Would you like to sign in again?"
msgstr "No se pudo sincronizar una cuenta. ¿Quiere acceder de nuevo?"

#: src/sync-daemon.cpp:651
msgid "Syncing %1 (Calendar)"
msgstr "Sincronizando %1 (calendario)"

#: src/sync-daemon.cpp:760
msgid ""
"Could not sync calendar %1 from account %2.\n"
"%3"
msgstr ""
"No se pudo sincronizar el calendario %1 desde la cuenta %2.\n"
"%3"

#: src/sync-daemon.cpp:780
msgid "Finished syncing %1 (Calendar)"
msgstr "Finalizó la sincronización de %1 (calendario)"

#~ msgid ""
#~ "Your access key is not valid anymore. Do you want to re-authenticate it?."
#~ msgstr "Su clave de acceso ya no es válida. ¿Desea volver a identificarse?"

#~ msgid ""
#~ "Your account failed to authenticate while syncing. Please click below to "
#~ "re-authenticate."
#~ msgstr ""
#~ "Su cuenta falló para autenticarse mientras sincronizaba. Por favor haga "
#~ "clic abajo para volver a autenticarse."
