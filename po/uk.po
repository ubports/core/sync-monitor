# Ukrainian translation for sync-monitor
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the sync-monitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: sync-monitor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 09:52+0000\n"
"PO-Revision-Date: 2023-02-18 17:33+0000\n"
"Last-Translator: Sergii Horichenko <m@sgg.im>\n"
"Language-Team: Ukrainian <https://hosted.weblate.org/projects/lomiri/sync-"
"monitor/uk/>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2016-09-04 07:28+0000\n"

#: accounts/plugin/generic-caldav.provider.in:3
msgid "Generic CalDAV"
msgstr "Загальний CalDAV"

#: accounts/plugin/qml/NewAccount.qml:15
msgid "Invalid host URL"
msgstr "Помилкова URL-адреса"

#: accounts/plugin/qml/NewAccount.qml:38
msgid "URL:"
msgstr "URL:"

#: accounts/plugin/qml/NewAccount.qml:44
msgid "http://myserver.com/caldav/"
msgstr "http://myserver.com/caldav/"

#: accounts/plugin/qml/NewAccount.qml:53
msgid "Username:"
msgstr "Ім'я користувача:"

#: accounts/plugin/qml/NewAccount.qml:59
msgid "Your username"
msgstr "Ваше ім'я користувача"

#: accounts/plugin/qml/NewAccount.qml:68
msgid "Password:"
msgstr "Пароль:"

#: accounts/plugin/qml/NewAccount.qml:74
msgid "Your password"
msgstr "Ваш пароль"

#: accounts/plugin/qml/NewAccount.qml:89
msgid "Cancel"
msgstr "Скасувати"

#: accounts/plugin/qml/NewAccount.qml:95
msgid "Continue"
msgstr "Далі"

#: accounts/services/generic-caldav.service.in:4
msgid "Generic Calendar"
msgstr "Загальний календар"

#: accounts/services/google-caldav.service.in:4
msgid "Google Calendar"
msgstr "Календар Google"

#: accounts/services/google-carddav.service.in:4
msgid "Google Contacts"
msgstr "Контакти Google"

#: accounts/services/nextcloud-caldav.service.in:4
#: accounts/services/owncloud-caldav.service.in:4
msgid "Calendar"
msgstr "Календар"

#: authenticator/main.qml:60 authenticator/main.qml:78
msgid "Accounts"
msgstr "Облікові записи"

#: authenticator/main.qml:82 authenticator/main.qml:130
msgid "Quit"
msgstr "Вийти"

#: authenticator/main.qml:88
msgid "Could not load account info."
msgstr "Не вдалося завантажити дані облікового запису."

#: authenticator/main.qml:126
msgid "Logging in…"
msgstr "Вхід до системи…"

#: authenticator/main.qml:126
msgid "Sign in to sync"
msgstr "Увійдіть для синхронізації"

#: authenticator/main.qml:153
msgid "Account sign-in failed. Please select your account to sign back in."
msgstr ""
"Увійти в обліковий запис не вдалося. Будь ласка, оберіть свій обліковий "
"запис для повторного входу."

#: src/notify-message.cpp:86
msgid "Yes"
msgstr "Так"

#: src/notify-message.cpp:91
msgid "No"
msgstr "Ні"

#: src/sync-account.cpp:735
msgid "Failed to configure account"
msgstr "Не вдалося налаштувати обліковий запис"

#: src/sync-account.cpp:744
msgid "Forbidden / access denied"
msgstr "Заборонено доступ"

#: src/sync-account.cpp:746
msgid "Object not found / unassigned field"
msgstr "Об’єкт не знайдено / не призначено поле"

#: src/sync-account.cpp:748
msgid "Command not allowed"
msgstr "Заборонена команда"

#: src/sync-account.cpp:751
msgid "Proxy authentication required"
msgstr "Потрібно пройти розпізнавання на проксі-сервері"

#: src/sync-account.cpp:753
msgid "Disk full"
msgstr "Диск переповнено"

#: src/sync-account.cpp:755
msgid "Could not sync due to a remote problem"
msgstr "Не вдалося синхронізувати через віддалену проблему"

#: src/sync-account.cpp:757
msgid "Could not run \"two-way\" sync"
msgstr "Не вдалося запустити \"двосторонню\" синхронізацію"

#: src/sync-account.cpp:759
msgid "Could not sync some items"
msgstr "Не вдалося синхронізувати деякі елементи"

#: src/sync-account.cpp:761
msgid "Process died unexpectedly"
msgstr "Процес несподівано зупинився"

#: src/sync-account.cpp:764
msgid "Server sent bad content"
msgstr "Сервером надіслано помилкові дані"

#: src/sync-account.cpp:766
msgid "Sync canceled"
msgstr "Синхронізація скасована"

#: src/sync-account.cpp:768
msgid "Connection timeout"
msgstr "Припустимий час очікування для з’єднання вичерпано"

#: src/sync-account.cpp:770
msgid "Connection certificate has expired"
msgstr "Строк дії сертифіката з’єднання вичерпано"

#: src/sync-account.cpp:772
msgid "Connection certificate is invalid"
msgstr "Некоректний сертифікат з’єднання"

#: src/sync-account.cpp:776
msgid "Could not connect to the server"
msgstr "Не вдалося з’єднатися з сервером"

#: src/sync-account.cpp:779
msgid "Server not found"
msgstr "Сервер не знайдено"

#: src/sync-account.cpp:781
msgid "Unknown status"
msgstr "Невідомий стан"

#: src/sync-daemon.cpp:627 src/sync-daemon.cpp:649 src/sync-daemon.cpp:759
#: src/sync-daemon.cpp:778
msgid "Synchronization"
msgstr "Синхронізація"

#: src/sync-daemon.cpp:628
msgid "An account failed to sync. Would you like to sign in again?"
msgstr ""
"Не вдалося синхронізувати обліковий запис. Бажаєте знову увійти в обліковий "
"запис?"

#: src/sync-daemon.cpp:651
msgid "Syncing %1 (Calendar)"
msgstr "Синхронізація %1 (Календар)"

#: src/sync-daemon.cpp:760
msgid ""
"Could not sync calendar %1 from account %2.\n"
"%3"
msgstr ""
"Не вдалося синхронізувати календар %1 з облікового запису %2.\n"
"%3"

#: src/sync-daemon.cpp:780
msgid "Finished syncing %1 (Calendar)"
msgstr "Завершено синхронізацію %1 (Календар)"

#~ msgid ""
#~ "Your access key is not valid anymore. Do you want to re-authenticate it?."
#~ msgstr ""
#~ "Ваш ключ доступу є недійсним. Ви бажаєте повторно виконати "
#~ "автентифікацію?."

#~ msgid ""
#~ "Your account failed to authenticate while syncing. Please click below to "
#~ "re-authenticate."
#~ msgstr ""
#~ "Не вдалося пройти розпізнавання для вашого облікового запису під час "
#~ "синхронізації. Будь ласка, натисніть нижчет, щоб спробувати пройти "
#~ "розпізнавання ще раз."
