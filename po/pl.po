# Polish translation for sync-monitor
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the sync-monitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: sync-monitor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 09:52+0000\n"
"PO-Revision-Date: 2023-02-18 17:33+0000\n"
"Last-Translator: gnu-ewm <gnu.ewm@protonmail.com>\n"
"Language-Team: Polish <https://hosted.weblate.org/projects/lomiri/sync-"
"monitor/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2016-09-09 06:34+0000\n"

#: accounts/plugin/generic-caldav.provider.in:3
msgid "Generic CalDAV"
msgstr "Ogólne CalDAV"

#: accounts/plugin/qml/NewAccount.qml:15
msgid "Invalid host URL"
msgstr "Nieprawidłowy adres URL hosta"

#: accounts/plugin/qml/NewAccount.qml:38
msgid "URL:"
msgstr "Adres URL:"

#: accounts/plugin/qml/NewAccount.qml:44
msgid "http://myserver.com/caldav/"
msgstr "http://myserver.com/caldav/"

#: accounts/plugin/qml/NewAccount.qml:53
msgid "Username:"
msgstr "Nazwa użytkownika:"

#: accounts/plugin/qml/NewAccount.qml:59
msgid "Your username"
msgstr "Twoja nazwa użytkownika"

#: accounts/plugin/qml/NewAccount.qml:68
msgid "Password:"
msgstr "Hasło:"

#: accounts/plugin/qml/NewAccount.qml:74
msgid "Your password"
msgstr "Twoje hasło"

#: accounts/plugin/qml/NewAccount.qml:89
msgid "Cancel"
msgstr "Anuluj"

#: accounts/plugin/qml/NewAccount.qml:95
msgid "Continue"
msgstr "Kontynuuj"

#: accounts/services/generic-caldav.service.in:4
msgid "Generic Calendar"
msgstr "Kalendarz ogólny"

#: accounts/services/google-caldav.service.in:4
msgid "Google Calendar"
msgstr "Kalendarz Google"

#: accounts/services/google-carddav.service.in:4
msgid "Google Contacts"
msgstr "Kontakty Google"

#: accounts/services/nextcloud-caldav.service.in:4
#: accounts/services/owncloud-caldav.service.in:4
msgid "Calendar"
msgstr "Kalendarz"

#: authenticator/main.qml:60 authenticator/main.qml:78
msgid "Accounts"
msgstr "Konta"

#: authenticator/main.qml:82 authenticator/main.qml:130
msgid "Quit"
msgstr "Wyjdź"

#: authenticator/main.qml:88
msgid "Could not load account info."
msgstr "Nie można załadować informacji o koncie."

#: authenticator/main.qml:126
msgid "Logging in…"
msgstr "Logowanie…"

#: authenticator/main.qml:126
msgid "Sign in to sync"
msgstr "Zaloguj się, aby zsynchronizować"

#: authenticator/main.qml:153
msgid "Account sign-in failed. Please select your account to sign back in."
msgstr ""
"Logowanie na konto nie powiodło się. Wybierz swoje konto, aby zalogować się "
"ponownie."

#: src/notify-message.cpp:86
msgid "Yes"
msgstr "Tak"

#: src/notify-message.cpp:91
msgid "No"
msgstr "Nie"

#: src/sync-account.cpp:735
msgid "Failed to configure account"
msgstr "Nie udało się skonfigurować konta"

#: src/sync-account.cpp:744
msgid "Forbidden / access denied"
msgstr "Zakazane / odmowa dostępu"

#: src/sync-account.cpp:746
msgid "Object not found / unassigned field"
msgstr "Nie znaleziono obiektu / pole nieprzypisane"

#: src/sync-account.cpp:748
msgid "Command not allowed"
msgstr "Niedozwolone polecenie"

#: src/sync-account.cpp:751
msgid "Proxy authentication required"
msgstr "Wymagane uwierzytelnienie pośrednika sieciowego"

#: src/sync-account.cpp:753
msgid "Disk full"
msgstr "Dysk jest pełny"

#: src/sync-account.cpp:755
msgid "Could not sync due to a remote problem"
msgstr "Nie można zsynchronizować z powodu problemu zdalnego"

#: src/sync-account.cpp:757
msgid "Could not run \"two-way\" sync"
msgstr "Nie można uruchomić synchronizacji dwukierunkowej"

#: src/sync-account.cpp:759
msgid "Could not sync some items"
msgstr "Nie udało się zsynchronizować niektórych elementów"

#: src/sync-account.cpp:761
msgid "Process died unexpectedly"
msgstr "Proces nieoczekiwanie umarł"

#: src/sync-account.cpp:764
msgid "Server sent bad content"
msgstr "Serwer przesłał złą treść"

#: src/sync-account.cpp:766
msgid "Sync canceled"
msgstr "Anulowano synchronizację"

#: src/sync-account.cpp:768
msgid "Connection timeout"
msgstr "Przekroczenie czasu połączenia"

#: src/sync-account.cpp:770
msgid "Connection certificate has expired"
msgstr "Certyfikat połączenia wygasł"

#: src/sync-account.cpp:772
msgid "Connection certificate is invalid"
msgstr "Certyfikat połączenia jest nieprawidłowy"

#: src/sync-account.cpp:776
msgid "Could not connect to the server"
msgstr "Nie można połączyć się z serwerem"

#: src/sync-account.cpp:779
msgid "Server not found"
msgstr "Nie odnaleziono serwera"

#: src/sync-account.cpp:781
msgid "Unknown status"
msgstr "Nieznany stan"

#: src/sync-daemon.cpp:627 src/sync-daemon.cpp:649 src/sync-daemon.cpp:759
#: src/sync-daemon.cpp:778
msgid "Synchronization"
msgstr "Synchronizacja"

#: src/sync-daemon.cpp:628
msgid "An account failed to sync. Would you like to sign in again?"
msgstr "Konto nie zostało zsynchronizowane. Czy chcesz zalogować się ponownie?"

#: src/sync-daemon.cpp:651
msgid "Syncing %1 (Calendar)"
msgstr "Synchronizowanie %1 (Kalendarz)"

#: src/sync-daemon.cpp:760
msgid ""
"Could not sync calendar %1 from account %2.\n"
"%3"
msgstr ""
"Nie udało się zsynchronizować kalendarza %1 z konta %2.\n"
"%3"

#: src/sync-daemon.cpp:780
msgid "Finished syncing %1 (Calendar)"
msgstr "Zakończono synchronizację %1 (Kalendarz)"

#~ msgid ""
#~ "Your access key is not valid anymore. Do you want to re-authenticate it?."
#~ msgstr ""
#~ "Twój klucz dostępu nie jest już ważny. Czy chcesz go ponownie "
#~ "uwierzytelnić?"

#~ msgid ""
#~ "Your account failed to authenticate while syncing. Please click below to "
#~ "re-authenticate."
#~ msgstr ""
#~ "Twoje konto nie zostało poprawnie uwierzytelnione podczas synchronizacji. "
#~ "Kliknij poniżej, aby ponownie uwierzytelnić."
