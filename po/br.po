# Breton translation for sync-monitor
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the sync-monitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: sync-monitor\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-17 09:52+0000\n"
"PO-Revision-Date: 2016-10-10 07:14+0000\n"
"Last-Translator: Fohanno Thierry <thierry.fohanno@ofis-bzh.org>\n"
"Language-Team: Breton <br@li.org>\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-10-11 07:38+0000\n"
"X-Generator: Launchpad (build 18227)\n"

#: accounts/plugin/generic-caldav.provider.in:3
msgid "Generic CalDAV"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:15
msgid "Invalid host URL"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:38
msgid "URL:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:44
msgid "http://myserver.com/caldav/"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:53
msgid "Username:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:59
msgid "Your username"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:68
msgid "Password:"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:74
msgid "Your password"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:89
msgid "Cancel"
msgstr ""

#: accounts/plugin/qml/NewAccount.qml:95
msgid "Continue"
msgstr ""

#: accounts/services/generic-caldav.service.in:4
msgid "Generic Calendar"
msgstr ""

#: accounts/services/google-caldav.service.in:4
msgid "Google Calendar"
msgstr ""

#: accounts/services/google-carddav.service.in:4
msgid "Google Contacts"
msgstr ""

#: accounts/services/nextcloud-caldav.service.in:4
#: accounts/services/owncloud-caldav.service.in:4
msgid "Calendar"
msgstr ""

#: authenticator/main.qml:60 authenticator/main.qml:78
msgid "Accounts"
msgstr "Kontoù"

#: authenticator/main.qml:82 authenticator/main.qml:130
msgid "Quit"
msgstr "Kuitaat"

#: authenticator/main.qml:88
#, fuzzy
#| msgid "Fail to load account information."
msgid "Could not load account info."
msgstr "N'eus ket bet gallet kargañ titouroù ar gont"

#: authenticator/main.qml:126
msgid "Logging in…"
msgstr ""

#: authenticator/main.qml:126
#, fuzzy
#| msgid "Fail to sync"
msgid "Sign in to sync"
msgstr "N'eus ket bet gallet sinkronelaat"

#: authenticator/main.qml:153
msgid "Account sign-in failed. Please select your account to sign back in."
msgstr ""

#: src/notify-message.cpp:86
msgid "Yes"
msgstr "Ya"

#: src/notify-message.cpp:91
msgid "No"
msgstr "Ket"

#: src/sync-account.cpp:735
#, fuzzy
#| msgid "Fail to configure account"
msgid "Failed to configure account"
msgstr "N'eus ket bet gallet kefluniañ ar gont"

#: src/sync-account.cpp:744
msgid "Forbidden / access denied"
msgstr "Difennet / moned nac'het"

#: src/sync-account.cpp:746
msgid "Object not found / unassigned field"
msgstr "Traezenn ha n'eo ket bet kavet / maezienn ha n'eo ket bet deroet"

#: src/sync-account.cpp:748
msgid "Command not allowed"
msgstr "Urzh diaotreet"

#: src/sync-account.cpp:751
msgid "Proxy authentication required"
msgstr ""

#: src/sync-account.cpp:753
msgid "Disk full"
msgstr "Pladenn leun"

#: src/sync-account.cpp:755
#, fuzzy
#| msgid "Fail to sync due some remote problem"
msgid "Could not sync due to a remote problem"
msgstr "N'eus ket bet gallet sinkronelaat abalamour d'un diaester a-bell"

#: src/sync-account.cpp:757
msgid "Could not run \"two-way\" sync"
msgstr ""

#: src/sync-account.cpp:759
#, fuzzy
#| msgid "Fail to sync some items"
msgid "Could not sync some items"
msgstr "N'eus ket bet gallet sinkronelaat elfennoù zo"

#: src/sync-account.cpp:761
msgid "Process died unexpectedly"
msgstr ""

#: src/sync-account.cpp:764
msgid "Server sent bad content"
msgstr "Ar servijer en deus kaset roadennoù direizh"

#: src/sync-account.cpp:766
msgid "Sync canceled"
msgstr "Sinkroneladur nullet"

#: src/sync-account.cpp:768
msgid "Connection timeout"
msgstr "Kevreadur kollet"

#: src/sync-account.cpp:770
msgid "Connection certificate has expired"
msgstr "Diamzeret eo an testeni kevreañ"

#: src/sync-account.cpp:772
msgid "Connection certificate is invalid"
msgstr "Direizh eo an testeni kevreañ"

#: src/sync-account.cpp:776
#, fuzzy
#| msgid "Fail to connect with the server"
msgid "Could not connect to the server"
msgstr "N'eus ket bet gallet kevreañ ouzh ar servijer"

#: src/sync-account.cpp:779
msgid "Server not found"
msgstr "N'eo ket bet kavet ar servijer"

#: src/sync-account.cpp:781
msgid "Unknown status"
msgstr "Statud dianav"

#: src/sync-daemon.cpp:627 src/sync-daemon.cpp:649 src/sync-daemon.cpp:759
#: src/sync-daemon.cpp:778
msgid "Synchronization"
msgstr "Sinkroneladur"

#: src/sync-daemon.cpp:628
msgid "An account failed to sync. Would you like to sign in again?"
msgstr ""

#: src/sync-daemon.cpp:651
#, fuzzy
#| msgid "Sync done: %1 (Calendar)"
msgid "Syncing %1 (Calendar)"
msgstr "Sinkroneladur echu : %1 (Deiziataer)"

#: src/sync-daemon.cpp:760
#, fuzzy
#| msgid ""
#| "Fail to sync calendar %1 from account %2.\n"
#| "%3"
msgid ""
"Could not sync calendar %1 from account %2.\n"
"%3"
msgstr ""
"N'eus ket bet gallet sinkronelaat an deiziataer %1 eus ar gont %2.\n"
"%3"

#: src/sync-daemon.cpp:780
#, fuzzy
#| msgid "Start sync: %1 (Calendar)"
msgid "Finished syncing %1 (Calendar)"
msgstr "Kregiñ da sinkronelaat : %1 (Deiziataer)"
